module.exports = {
    apiKey: 'SnesqlCsa5CqgzGnn99JOk3V105L2P109HN1bl2XcZ6yfDWs110',
    //
    browser: [
        // Add browsers with different viewports
        {width: 800, height: 600, name: 'chrome'},
        {width: 700, height: 500, name: 'firefox'},
//        {width: 1600, height: 1200, name: 'ie11'},
//        {width: 1024, height: 768, name: 'edgechromium'},
        {width: 800, height: 600, name: 'safari'},
//        // Add mobile emulation devices in Portrait mode
//        {deviceName: 'iPhone X', screenOrientation: 'portrait'},
    ],
    // set batch name to the configuration
    batchName: 'Test Cafe UltraFast Batch',
    batchId: process.env.APPLITOOLS_BATCH_ID||"My Batch ID",
    accessibilitySettings: {level: 'AA', guidelinesVersion: 'WCAG_2_0'},
    parentBranchName:"default"
}